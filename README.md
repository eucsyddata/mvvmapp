## MVVM demo ##

### Branch: 1.BasicViewModel ###

ViewModel can be instantiated in the constructor of the View, 
which also set the BindingContext to VM-object.
Or it can be instantiated direct i XAML.

No function behind the buttons. It is not possible to place 
the eventhandlers in code behind. We need Command Pattern!